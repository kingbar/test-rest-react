import csv
import json
import pandas as pd

from django.core.management.base import BaseCommand

from apps.service.models import Standard


class Command(BaseCommand):
    help = "save data from .csv"

    def handle(self, *args, **kwargs):
        df = pd.read_csv('csv/data.csv', delimiter=';')

        def get_nested_record(key, grp):
            rec = {}
            rec['standard'] = 'Michigan Integrated Technology Competencies for Students'
            rec['definitions'] = list()
            for grade in list(grp['GRADE'].unique()):
                rec['definitions'].append({grade: []})

            for i, grade in enumerate(rec['definitions']):
                for k, v in grade.items():
                    for domain in list(grp['LEARNING_DOMAIN'].unique()):
                        rec['definitions'][i][k].append({
                            domain: []
                        })

            for i, grade in enumerate(rec['definitions']):
                for k, v in grade.items():
                    for j, domain in enumerate(grade[k]):
                        for key, val in domain.items():
                            for i, code in enumerate(list(grp['FULL_CODE'])):
                                domain[key].append({
                                    'full_code': code,
                                    'description': grp['DESCRIPTION'][i],
                                })
            return rec

        result = []
        for key, grp in df.groupby(['STANDARD']):
            rec = get_nested_record(key, grp)
            result.append(rec)

        Standard.objects.create(data=result)
