import React from 'react';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import Test from '../components/Test'


class Home extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      data: null
    }
  }

  render() {
    return (
      <Test />
    )
  }
}

export default withRouter(connect()(Home))
