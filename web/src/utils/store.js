import { createStore, combineReducers, applyMiddleware } from 'redux'
import reducer from './reducers'
import thunk from 'redux-thunk'

const exportReducer = combineReducers({
 reducer
})
const store = createStore(
 exportReducer,
 applyMiddleware(thunk)
)

export default store
