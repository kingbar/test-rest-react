from django.contrib.postgres.fields import JSONField
from django.db import models


class Standard(models.Model):
    data = JSONField()

    def __str__(self):
        return str(self.id)
