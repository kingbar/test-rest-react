from django.urls import include, path

urlpatterns = [

    # tracker endpoints
    path('v1/rest-auth/', include('rest_auth.urls')),
    path(
        'v1/rest-auth/registration/',
        include('rest_auth.registration.urls')
    )

    # dashboard endpoints
]
