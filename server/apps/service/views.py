from django.shortcuts import render
from django.http import HttpResponse
import json

from .models import Standard
from .serializers import StandardSerializer

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny


@api_view(['GET'])
@permission_classes((AllowAny,))
def get_data(request):
    standard = Standard.objects.last().data

    return HttpResponse(
        json.dumps({'data': standard}),
        content_type='application/json'
    )
