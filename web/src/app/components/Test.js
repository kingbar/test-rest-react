import React from 'react';
import axios from '../../axios';

class Test extends React.PureComponent {
  state = {
    standard: '',
    level: '',
    domain: '',
    tag: '',
    data: {}
  };

  componentDidMount() {
   axios.get(`/get-standard/`)
     .then(res => {
       const data = res.data.data[0];
       this.setState({ data });
     })
 }

  handleStandardChange = event => {
    this.setState({ [event.target.name]: event.target.value, level: '', domain: '', tag: '' });
  };
  handleLevelChange = event => {
    this.setState({ [event.target.name]: event.target.value, domain: '', tag: '' });
  };
  handleDomainChange = event => {
    this.setState({ [event.target.name]: event.target.value, tag: '' });
  };
  handleTagChange = (event, code) => {
    const full_code = Object.values(Object.values(this.state.data.definitions[this.state.level])[0][this.state.domain])[0]
      .find(code => code.description === event.target.value).full_code
    this.setState({ [event.target.name]: event.target.value, fullCode: full_code });
    console.log()
  };

  render() {
    const modalStyle = {
      margin: '50px',
      padding: '50px',
      backgroundColor: 'rgba(214, 214, 214, 0.24)',
    }
    const headerStyle = {
      padding: '10px',
      backgroundColor: 'white',
      marginBottom: '50px'
    }
    return (
        <div className='container'>
          <div className='modal-content' style={modalStyle}>
            {this.state.fullCode ?
              <div style={headerStyle}>
                <h5 className="text-center">
                  <strong>{this.state.fullCode}</strong>
                </h5>
                {
                  this.state.tag ?
                    <p className='text-left'><strong>Description: </strong>{this.state.tag}</p>
                  : null
                }
              </div>
            : null
            }
            <h4 className="text-center">
              Add New Aligment Tag
            </h4>
            <div className="form-group">
              <label htmlFor="standard">Education Standard</label>
              <select
                value={this.state.standard}
                onChange={this.handleStandardChange}
                name='standard'
                className="form-control"
              >
                <option value='' hidden>Select</option>
                <option value={0}>{this.state.data.standard}</option>
              </select>
            </div>
            {this.state.standard ?
              <div className="form-group">
                <label htmlFor="level">Grade Level</label>
                <select
                  value={this.state.level}
                  onChange={this.handleLevelChange}
                  name='level'
                  className="form-control"
                >
                  <option value='' hidden>Select</option>
                  {this.state.data.definitions.map((definition, index) => {
                    return <option value={index} key={index}>Grade-{index}</option>
                  })}
                </select>
              </div>
            : null}
            {this.state.level ?
              <div className="form-group">
                <label htmlFor="domain">Learning Domain</label>
                <select
                  value={this.state.domain}
                  onChange={this.handleDomainChange}
                  name='domain'
                  className="form-control"
                >
                  <option value='' hidden>Select</option>
                  {
                    Object.values(this.state.data.definitions[this.state.level]).map((domain, index) => {
                      return(
                        domain.map((sub, index) => {
                          return <option key={index} value={index}>{Object.keys(sub)[0]}</option>
                        }))
                    })
                  }
                </select>
              </div>
            : null }
            {this.state.domain ?
              <div className="form-group">
                <label htmlFor="tag">Aligment Tag</label>
                <select
                  value={this.state.tag}
                  onChange={this.handleTagChange}
                  name='tag'
                  className="form-control"
                >
                  <option value='' hidden>Select</option>
                  {Object.values(Object.values(this.state.data.definitions[this.state.level])[0][this.state.domain]).map((code, index) => {
                    return(
                      code.map((fullCode, index) => {
                        return <option key={index} code={fullCode.full_code} value={fullCode.description}>{fullCode.full_code}</option>
                      }))
                  })}
                </select>
              </div>
            : null}
          </div>
        </div>
    )
  }
}

export default Test;
