from django.contrib.auth.models import AbstractUser
from django.db import models

from .managers import UserManager

# Create your models here.


class User(AbstractUser):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    username = models.CharField(max_length=1, null=True, blank=True)
    job_title = models.CharField(max_length=255, null=True, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['job_title']

    def __str__(self):
        return self.email
