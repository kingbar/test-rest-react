import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';
import Home from '../app/pages/Home';

class Test extends React.Component {
  render() {
    return (
      <Fragment>
        <Route path="/" component={Home} exact/>
      </Fragment>
    )
  }
}


export default Test
