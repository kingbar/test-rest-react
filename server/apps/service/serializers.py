from rest_framework import serializers

from .models import Standard


class StandardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Standard
        fields = ('data',)
