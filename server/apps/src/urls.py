from django.contrib import admin
from django.urls import include, path

from apps.router import router
from rest_auth.views import LoginView
from apps.service.views import get_data

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path(
        'api/v1/api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    ),
    path('api/', include(router.urls)),
    path('login/', LoginView.as_view(), name='login'),
    path('get-standard/', get_data)
]
