# Django-gulp boilerplate

## What's included?

- [Django v2.1](https://docs.djangoproject.com/en/1.11/)
- [Django Rest Framework](http://www.django-rest-framework.org/)
- Python 3.5 based [Dockerfile](https://hub.docker.com/_/python/)
- Node 8 [docker image](https://hub.docker.com/_/node/)
- [Docker compose](https://docs.docker.com/compose/)
- [Sass](http://sass-lang.com/)
- Postgresql 9.6 database
- Template structure

## How to setup project?

1.  Setup .env file according to .env.example
2.  Build project `docker-compose build`
3.  Run twice (first - creating, second - starting) database container `docker-compose up -d database`
    - Enter to database bash `docker-compose run database bash`
    - `psql -h database -U postgres`
    - Create database according to .env file `CREATE USER tracker WITH SUPERUSER PASSWORD 'tracker';`
    - Create database according to .env file `CREATE DATABASE tracker WITH OWNER tracker;`
    - Leave database container `exit`
4.  Install node dependencies `docker-compose run node yarn`
5.  Run backend `docker-compose run django bash`
    - Run migrations `python manage.py migrate`
    - Leave backend container `exit`
6.  Populate db from csv `docker-compose run django python manage.py save_data`

## How to run project?

```bash
docker-compose up
```

## Project Structure

- **./backend/** Django web server
  - **./api/** API application
- **./frontend/** Main frontend folder
  - **./assets/** Assets folder
    - **./js/** Javascript (including ES2015 syntax)
    - **./scss/** Sass
    - **./styl/** Stylus
    - **./fonts/** Fonts
    - **./images/** Images
  - **./dist/** Results folder
