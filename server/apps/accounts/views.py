import json

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny

from .models import User
from .serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


@csrf_exempt
@api_view(['POST'])
@permission_classes((AllowAny,))
def update_user(request, id=None):
    user = User.objects.get(pk=id)
    user.email = request.data.get('email')
    user.first_name = request.data.get('first_name')
    user.last_name = request.data.get('last_name')
    user.job_title = request.data.get('job_title')
    user.save()

    return HttpResponse(
        json.dumps({'user': UserSerializer(user).data}),
        content_type='application/json'
    )
