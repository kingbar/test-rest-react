import { LOGIN_SUCCESS } from './constants'
import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

const reducer = ( state = {}, action ) => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      return {...state, token: action.payload, is_authenticated: true}
    }
    default:
      return state
  }
}

export default combineReducers({
  reducer,
  form: formReducer
})
