import React, { Component } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import { Provider } from 'react-redux'
import store from './utils/store'
import Test from './containers/Test'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Test />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
