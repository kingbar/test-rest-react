import { LOGIN_SUCCESS } from './constants'


export const login = data => {
  return dispatch => {
    dispatch({
      type: LOGIN_SUCCESS,
      payload: data
    })
  }
}
